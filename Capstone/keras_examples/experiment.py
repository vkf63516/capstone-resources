from Classifier import *
from Dataset import *
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

def run_special_binary_text_classification_dataset():
    #load the dataset
    data_filepath1 = '../JSONTweets/english_test_with_labels.csv'

    # data_filepath = "/home/vishalfenn/Downloads/ade_tweets.tsv"
    data = Special_Binary_Text_Classification_Dataset(data_filepath1, train_set_size=0.7, validation_set_size=0.5)
    model_in_file_name = '../models/'
    model_out_file_name = '/models5/fine_data1'
    #create classifier and load data for a binary text classifier
    language_model_name = Classifier.ROBERTA_TWITTER
    num_classes = 8
    max_length = 1084
    max_epoch = 12
    batch_size = 32
    learning_rate = 0.01
    dropout_rate = 0.7
    early_stopping_monitor = 'val_loss'
    classifier = Binary_Text_Classifier(language_model_name,
                                        max_length=max_length,
                                        learning_rate=learning_rate,
                                        dropout_rate=dropout_rate)
    
    #get the training data
    train_x, train_y = data.get_train_data()
    val_x, val_y = data.get_validation_data()
    test_x, test_y = data.get_test_data()
    
    #train the model

    #evaluate the test set
    
    # classifier.load_weights(model_in_file_name)
    # classifier.save_weights(model_out_file_name)
    # If you want to save model weights, use below.
    classifier.model.summary()
    #plot the model (an image)
    tf.keras.utils.plot_model(
        classifier.model,
        to_file="model.png",
        show_shapes=True,
        show_dtype=True,
        show_layer_names=True,
        rankdir="TB",
        expand_nested=False,
        dpi=96,
    )
    
    # print(classifier.predict(test_x))
    # classifierp
    classifier.train(train_x, train_y,
                     validation_data=(val_x, val_y),
                     epochs=max_epoch,
                     batch_size=batch_size,
                    #  mode_out_file_name=model_out_file_name,
                     early_stopping_patience = 5, early_stopping_monitor=early_stopping_monitor,
                     class_weights = data.get_train_class_weights()
        
    )
    classifier.save_weights(model_out_file_name)
    # y_pred = classifier.predict(test_x)
    # acc = accuracy_score(y_pred, test_y)
    # print('Test Accuracy:', acc)
    results = classifier.evaluate(test_x, test_y)
    print(f"test loss {results[0]}, test accuracy: {results[1]}, test precision: {results[2]}, test recall: {results[3]}")
   

    # pred_Y = classifier.predict(test_x)
    # for i in range(len(test_y)):
      #   print("Y=%s, Predicted=%s" % (test_y[i], pred_Y[i]))
    # classifier.save_weights(model_out_file_name)

def run_trained_binary_special():
    model_in_file_name = '../models5/fine_data1'
    # model_out_file_name = '/content/drive/My Drive/Colab_Notebooks/Capstone/models1/fine_data1'
    #create classifier and load data for a binary text classifier
    language_model_name = Classifier.ROBERTA_TWITTER
    num_classes = 8
    max_length = 1084
    learning_rate = 0.01
    dropout_rate = 0.7
    classifier = Binary_Text_Classifier(language_model_name, max_length=max_length, learning_rate=learning_rate, dropout_rate=dropout_rate)
    classifier.load_weights(model_in_file_name)
    data_filepath1 = '../JSONTweets/climate_change_tweet.csv'
    df = pd.read_csv(data_filepath1).dropna()
    data = df['text'].values.tolist()
    threshold = 0.1
    predictions = classifier.predict(data)
    for i in range(len(data)):
      strx = ''
      x = int(predictions[i] > threshold)
      if x == 0:
        strx = 'misinformation'
      if x == 1:
        strx = 'not misinformation'
      print(data[i], strx, predictions[i])

# Simple example to test binary text classification datasets
# def run_binary_text_classification_dataset():
#     #load the dataset
#     # data_filepath = "/home/vishalfenn/Downloads/ade_tweets.tsv"
#     data = Binary_Text_Classification_Dataset(data_filepath, validation_set_size=0.2)
#
#     #create classifier and load data for a binary text classifier
#     language_model_name = Classifier.ROBERTA_TWITTER
#     num_classes = 8
#     max_length = 768
#     classifier = Binary_Text_Classifier(language_model_name, max_length=max_length)
#
#     #get the training data
#     train_x, train_y = data.get_train_data()
#     val_x, val_y = data.get_validation_data()
#
#     #train the model
#     # If you want to save model weights, use below.
#     classifier.train(train_x, train_y,
#                      validation_data=(val_x, val_y),
#                      class_weights = data.get_train_class_weights()
#     )

# TODO - I don't think I have a multiclass text classification dataset

    
# Simple example to test multilabel text classification datasets
def run_multilabel_text_classification_dataset():
    #load the dataset
    data_filepath = '../data/i2b2_relex/i2b2_converted.tsv'
    data = MultiLabel_Text_Classification_Dataset(data_filepath, validation_set_size=0.2)

    #create classifier and load data for a multiclass text classifier
    language_model_name = Classifier.BLUE_BERT_PUBMED_MIMIC
    num_classes = 8
    classifier = MultiLabel_Text_Classifier(language_model_name, num_classes)
    
    #get the training data
    train_x, train_y = data.get_train_data()
    val_x, val_y = data.get_validation_data()

    #train the model
    # If you want to save model weights, use below.
    classifier.train(train_x, train_y,
                     validation_data=(val_x, val_y),
                     class_weights = data.get_train_class_weights()
    )

# def run_special_multiclass_token_classification_dataset():
#     #load the dataset
#     data_filepath = '../JSONTweets/tweets.csv'
#     data = Special_MultiClass_Text_Classification(data_filepath, validation_set_size=0.2)
#
#     #create classifier and load data for a multiclass text classifier
#     language_model_name = Classifier.ROBERTA_TWITTER
#     num_classes = 3
#     classifier = MultiClass_Text_Classifier(language_model_name, num_classes)
#
#     #get the training data
#     train_x, train_y = data.get_train_data()
#     val_x, val_y = data.get_validation_data()
#
#     #train the model
#     #Note: class_weights are not supported for 3D targets, so we can't do it for token classification, at least not how we currently have it set up
#     classifier.train(train_x, train_y,
#                      validation_data=(val_x, val_y)
#     )

def run_multiclass_token_classification_dataset(): 
    #load the dataset
    data_filepath = '../data/i2b2_ner/training_data.tsv'
    data = Token_Classification_Dataset(data_filepath, validation_set_size=0.2)

    #create classifier and load data for a multiclass text classifier
    language_model_name = Classifier.BIODISCHARGE_SUMMARY_BERT
    num_classes = 3
    classifier = MultiClass_Token_Classifier(language_model_name, num_classes)
    
    #get the training data
    train_x, train_y = data.get_train_data()
    val_x, val_y = data.get_validation_data()

    #train the model
    #Note: class_weights are not supported for 3D targets, so we can't do it for token classification, at least not how we currently have it set up
    classifier.train(train_x, train_y,
                     validation_data=(val_x, val_y)       
    )






#This is the main running method for the script
if __name__ == '__main__':
    

    # Run these for debugging
    #run_complex_example()
    # run_binary_text_classification_dataset()
    #run_multilabel_text_classification_dataset()
    # run_special_binary_text_classification_dataset()
    run_trained_binary_special()
    # run_special_multiclass_token_classification_dataset()
    #run_essays_dataset()
    # run_i2b2_dataset()